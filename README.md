PelisApp - Ejercicio Técnico
Created by Javier Guzmán Serrano

Objetivo
Crear una aplicación de evaluación técnica cubriendo los requerimientos:
- Películas y/o Series categorizadas por Popular y Top Rated.
- Detalle de Película y/o Serie.
- Buscador de Películas y/o Series por nombre.
- Visualización de Videos en el detalle.
- La App debe poder funcionar offline.
- Pruebas Unitarias.
- Transiciones/Animaciones.
- Utilizar API: https://developers.themoviedb.org/
- Usar Kotlin.
- No hay restricción de bibliotecas externas.
- Subir el proyecto a Github/Bitbucket/Gitlab o cualquier otro que utilice Git.
- Arquitectura MVVM


Enlace Bitbucket
git clone https://JavierGuz@bitbucket.org/JavierGuz/pelisapp.git
https://bitbucket.org/JavierGuz/pelisapp/get/b0a26988d4c7.zip


Requerimientos no realizados:
-	Visualización de Videos en el detalle.
La propuesta es utilizar la API de youtube para obtener y reproducir el video 
Los pasos a seguir son:
1.	Localizar el objeto com.google.android.youtube.player.YouTubePlayerView
2.	Inicializar el objeto youtubeplayer con el api key
3.	Agregar los métodos onInitializationSuccess, onInitializationFailure
4.	Agregar validaciones (reproductor de youtube, reproducción en primer plano ) y asignar video
if(player==null) return
if (wasRestored){
	player.play()
}else{
	player.cueVideo("8Qn_spdM5Zg")
	player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL)
}

-	Guardar imágenes en el dispositivo 
Una vez utilizada la librería Picasso, que obtiene la imagen de una forma sencilla, implementar una clase tipo Target para guardar la imagen dentro del método .into (de picasso), la clase debe sobre escribir los métodos onBitmapLoaded, onBitmapFailed y onPrepareLoad y dentro de onBitmapLoaded guardar el bitmap descargado

-	Pruebas Unitarias 
En el propio IDE de AndroidStudio es posible crear local-tests.
En la ruta “app/java/mx.pelis(test)” se crearía un clase-test por cada una de las clases utilizadas en el programa y en cada una de esta se evaluarían casos de uso para comprobar la funcionalidad de cada método
