package mx.guzman.pelisapp.ui.view

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mx.guzman.pelisapp.R
import mx.guzman.pelisapp.data.model.db.Movie
import mx.guzman.pelisapp.data.model.db.MovieDatabase
import mx.guzman.pelisapp.data.model.db.MovieRepository
import mx.guzman.pelisapp.databinding.ActivityMainBinding
import mx.guzman.pelisapp.ui.viewmodel.MovieViewModel
import mx.guzman.pelisapp.ui.viewmodel.MovieViewModelFactory
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var movieViewModel: MovieViewModel
    private lateinit var adapter:MovieRecycleViewAdapter
    private var tag:String = "PelisApp-Main"
    private var vibrator:Vibrator?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val dao = MovieDatabase.getInstance(application).movieDAO
        val repository = MovieRepository(dao)
        val factory = MovieViewModelFactory(repository)
        movieViewModel = ViewModelProvider(this, factory).get(MovieViewModel::class.java)
        binding.mainViewModel = movieViewModel
        binding.lifecycleOwner = this
        initRecyclerView()

        movieViewModel.isLoading.observe(this, Observer { isLoading->
            binding.layProgress.isVisible = isLoading
        } )

        movieViewModel.message.observe(this, {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
        })
        binding.btnRefresh.setOnClickListener(View.OnClickListener {
            if(isInternetConnected()) {
                movieViewModel.refreshList()
            }else{
                Toast.makeText(this, "No existe una conexión a internet", Toast.LENGTH_SHORT).show()
            }
        })

        vibrator=getSystemService(VIBRATOR_SERVICE) as Vibrator?
        binding.btnSearch.setOnClickListener {
            if(binding.txtInputBuscar.isVisible){
            }else {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
                    vibrator?.vibrate(VibrationEffect.createOneShot(3,10))
                }else{
                    vibrator?.vibrate(3)
                }
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                var fadeSalir = AnimationUtils.loadAnimation(baseContext, R.anim.fade_in)
                binding.movieRecyclerView.startAnimation(fadeSalir)
                binding.movieRecyclerView.setVisibility(View.GONE)

                binding.txtInputBuscar.startAnimation(fadeEntrar)
                binding.txtInputBuscar.setVisibility(View.VISIBLE)
            }
        }
        binding.btnPopular.setOnClickListener {
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
                vibrator?.vibrate(VibrationEffect.createOneShot(10,10))
            }else{
                vibrator?.vibrate(3)
            }
            if(binding.txtInputBuscar.isVisible){
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                var fadeSalir = AnimationUtils.loadAnimation(baseContext, R.anim.fade_in)
                binding.movieRecyclerView.startAnimation(fadeEntrar)
                binding.movieRecyclerView.setVisibility(View.VISIBLE)
                binding.txtInputBuscar.startAnimation(fadeSalir)
                binding.txtInputBuscar.setVisibility(View.GONE)
            }else {
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                binding.movieRecyclerView.startAnimation(fadeEntrar)
                binding.movieRecyclerView.setVisibility(View.VISIBLE)
            }
            binding.txtBuscar.setText("")
            displayMoviesListOption(2, "")
        }
        binding.btnTop.setOnClickListener {
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
                vibrator?.vibrate(VibrationEffect.createOneShot(100,10))
            }else{
                vibrator?.vibrate(3)
            }
            if(binding.txtInputBuscar.isVisible){
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                var fadeSalir = AnimationUtils.loadAnimation(baseContext, R.anim.fade_in)
                binding.movieRecyclerView.startAnimation(fadeEntrar)
                binding.movieRecyclerView.setVisibility(View.VISIBLE)
                binding.txtInputBuscar.startAnimation(fadeSalir)
                binding.txtInputBuscar.setVisibility(View.GONE)
            }else {
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                binding.movieRecyclerView.startAnimation(fadeEntrar)
                binding.movieRecyclerView.setVisibility(View.VISIBLE)
            }
            displayMoviesListOption(1, "")
            binding.txtBuscar.setText("")
        }

    }

    private fun displayMoviesList(){

        movieViewModel.movies.observe(this, Observer {
            if(it.size == 0){
                callWebService();
            }
            initAutoCompleteInput(it)
            adapter.setList(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun displayMoviesListOption(option:Int, searchedName:String){

        movieViewModel.movies.observe(this, Observer {
            var list = it
            if(it.size == 0){

            }else{
                list = movieViewModel.getListOption(option, it, searchedName);
            }
            initAutoCompleteInput(list)
            adapter.setList(list)
            adapter.notifyDataSetChanged()
        })
    }



    private val uiScope = CoroutineScope(Job() + Dispatchers.Main)
    fun callWebService(){
        uiScope.launch (Dispatchers.IO){

            if(isInternetConnected()){
                movieViewModel.callWebService()
            }else{

            }
        }
    }
    private fun isInternetConnected():Boolean{
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

    private fun listItemClicked(movie: Movie){
        //desaparecer teclado
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.txtBuscar.getWindowToken(), 0)
        //
        val bottomSheetDialog = BottomSheetDialog(
            this@MainActivity, R.style.BottomSheetDialogTheme
        )
        val bottomSheetView = LayoutInflater.from(this).inflate(
            R.layout.layout_bottom_sheet,
            findViewById<LinearLayout>(R.id.bottomSheet)
        )
        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetDialog.show()
        bottomSheetView.findViewById<TextView>(R.id.txtTitulo).text="${movie.title}"
        bottomSheetView.findViewById<TextView>(R.id.txtNombre).text="${movie.title}"
        bottomSheetView.findViewById<TextView>(R.id.txtDescripcion).text="${movie.overview}"
        bottomSheetView.findViewById<TextView>(R.id.txtRank).text="${movie.vote_average}"

    }
    private fun initAutoCompleteInput(movies: List<Movie>){

        val palabrasClave: MutableList<String> = ArrayList()
        movies.forEach{ mov ->
            palabrasClave.add(mov.title)
        }
        var adapterBuscar = ArrayAdapter(
            this,
            R.layout.item_text,
            palabrasClave
        )
        binding.txtBuscar.setAdapter(adapterBuscar)

        binding.txtBuscar.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                true
            } else {
                //vibrar
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
                    vibrator?.vibrate(VibrationEffect.createOneShot(3,10))
                }else{
                    vibrator?.vibrate(3)
                }

                //desaparecer teclado
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(binding.txtBuscar.getWindowToken(), 0)

                //aparecer recyclerview
                var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
                binding.movieRecyclerView.startAnimation(fadeEntrar)
                binding.movieRecyclerView.setVisibility(View.VISIBLE)
                displayMoviesListOption(3, binding.txtBuscar.text.toString())

                false
            }
        }
    }
    private fun initRecyclerView(){
        var fadeEntrar = AnimationUtils.loadAnimation(baseContext, R.anim.fade_out)
        binding.movieRecyclerView.startAnimation(fadeEntrar)
        binding.movieRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = MovieRecycleViewAdapter ({ selectedItem:Movie->listItemClicked(selectedItem) })
        binding.movieRecyclerView.adapter = adapter
        displayMoviesList()
    }
}