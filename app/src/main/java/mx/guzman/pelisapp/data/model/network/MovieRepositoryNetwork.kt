package mx.guzman.pelisapp.data.model.network

import android.util.Log

class MovieRepositoryNetwork {
    private val api = MovieServiceNetwork()

    suspend fun getAllMovies(): MovieModelNetwork{
        val response: MovieModelNetwork = api.getMoviesNetwork()
        if(response.total_results == 0){
            MovieProvider.movies = emptyList()
        }else{
            MovieProvider.movies = response.results
        }
        return response
    }
}