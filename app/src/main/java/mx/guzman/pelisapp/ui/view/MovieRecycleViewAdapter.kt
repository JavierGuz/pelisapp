package mx.guzman.pelisapp.ui.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import mx.guzman.pelisapp.R
import mx.guzman.pelisapp.data.model.db.Movie
import mx.guzman.pelisapp.databinding.MovieListItemBinding
import java.lang.Exception


class MovieRecycleViewAdapter(private val clickListener:(Movie)->Unit)
    :RecyclerView.Adapter<ItemMovieViewHolder>(){
    
    private val moviesList = ArrayList<Movie>()
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMovieViewHolder {
        val layoutInflater:LayoutInflater = LayoutInflater.from(parent.context)
        val binding: MovieListItemBinding = 
            DataBindingUtil.inflate(layoutInflater, R.layout.movie_list_item, parent, false)
        return ItemMovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemMovieViewHolder, position: Int) {
        holder.bind(moviesList[position], clickListener)
    }

    override fun getItemCount(): Int {
        return moviesList.size
    }

    fun setList(movies: List<Movie>){
        moviesList.clear()
        moviesList.addAll(movies)
    }

}

class ItemMovieViewHolder(val binding: MovieListItemBinding): RecyclerView.ViewHolder(binding.root){
    fun bind(movie: Movie, clickListener: (Movie)->Unit){
        binding.titleTextView.text = movie.title
        binding.popularityTextView.text = movie.popularity.toString()
        binding.voteTextView.text = movie.vote_average.toString()
        //Log.i("PelisApp-ViewAdapter", "Url:https://image.tmdb.org/t/p/w500/"+movie.poster_path)
        Picasso.get()
            .load("https://image.tmdb.org/t/p/w500/"+movie.poster_path)
            .placeholder(R.drawable.loading_img_button)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .into(binding.imgMovie, object :Callback{
                override fun onSuccess() {

                }
                override fun onError(e: Exception?) {
                    Picasso.get()
                        .load("https://image.tmdb.org/t/p/w500/"+movie.poster_path)
                        .placeholder(R.drawable.loading_img_button)
                        .into(binding.imgMovie)
                }
            })

        binding.listItemLayout.setOnClickListener {
            clickListener(movie)
        }
    }
}