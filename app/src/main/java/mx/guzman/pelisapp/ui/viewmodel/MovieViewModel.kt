package mx.guzman.pelisapp.ui.viewmodel

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mx.guzman.pelisapp.data.model.db.Movie
import mx.guzman.pelisapp.data.model.db.MovieRepository
import mx.guzman.pelisapp.data.model.network.GetMoviesNetwork
import mx.guzman.pelisapp.data.model.network.MovieModelNetwork
import mx.guzman.pelisapp.data.model.network.netmodel.Level1ModelNetworkMovies
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


class MovieViewModel(private val repository: MovieRepository): ViewModel() {

    private var tag:String = "PelisApp-MovieViewModel"
    val movies = repository.movies
    val isLoading = MutableLiveData<Boolean>()

    val getMoviesNetwork = GetMoviesNetwork()

    //Custom Msg
    private val statusMessage = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>>
        get() = statusMessage


    init {

    }

    fun getDbMoviesArray(items:List<Level1ModelNetworkMovies>):Boolean{
        val success:Boolean = true
        items.forEach {
            insert(Movie(0, it.id, it.media_type, it.overview, it.popularity, it.poster_path, it.video, it.title,it.vote_average, it.vote_count))
        }
        return success
    }
    fun insert(movie: Movie) = viewModelScope.launch{
        val newRowId = repository.insert(movie)
    }
    fun clearAll(): Job = viewModelScope.launch{
        val noOfRowsDeleted =  repository.deleteAll()
        if(noOfRowsDeleted > 0){
            //Datos borrados
        }else{
            //Error al borrar datos
        }

    }
    fun callWebService(){
        clearAll()
        viewModelScope.launch {
            isLoading.postValue(true)
            val result: MovieModelNetwork? = getMoviesNetwork()
            if (result!!.total_results != 0)
            {
                val dbMoviesArray: Boolean = getDbMoviesArray(result.results)
                if (dbMoviesArray) {
                    //statusMessage.value = Event("Movies Inserted Successfully")
                } else {
                    //statusMessage.value = Event("Error Inserting Occurred")
                }

                statusMessage.value = Event("Datos consultados")
            } else {
                statusMessage.value = Event("Error al consultar los datos")
            }
            isLoading.postValue(false)
        }
    }
    fun refreshList(){
        clearAll()
    }
    fun getListOption(option: Int, list: List<Movie>, nameSearched:String): List<Movie> {
        var sortedMovies = list
        if(option < 3) {
            if (option == 1) {
                sortedMovies = list.sortedBy {
                    it.vote_average
                }
            } else if (option == 2) {
                sortedMovies = list.sortedBy {
                    it.popularity
                }
            }
            sortedMovies = sortedMovies.asReversed()
        }else{
            var array = ArrayList<Movie>()
            list.forEach {
                if (nameSearched.toLowerCase().contains(it.title.toLowerCase()) || it.title.toLowerCase().contains(nameSearched.toLowerCase())){
                    array.add(it)
                }
            }
            sortedMovies = array.toList()
        }
        return sortedMovies
    }

}