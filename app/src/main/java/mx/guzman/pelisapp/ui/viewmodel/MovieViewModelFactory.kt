package mx.guzman.pelisapp.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import mx.guzman.pelisapp.data.model.db.MovieRepository
import java.lang.IllegalArgumentException

class MovieViewModelFactory(
        private val repository: MovieRepository
    ): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MovieViewModel::class.java)){
            return MovieViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown View Model class")
    }
}