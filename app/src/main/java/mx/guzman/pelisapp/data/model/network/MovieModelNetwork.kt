package mx.guzman.pelisapp.data.model.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import mx.guzman.pelisapp.data.model.network.netmodel.Level1ModelNetworkMovies

class MovieModelNetwork (

        @SerializedName("total_results") val total_results:Int,
        @SerializedName("total_pages") val total_pages:Int,
        @SerializedName("page") val page:Int,
        @SerializedName("results") val results:List<Level1ModelNetworkMovies>

    ){
}
