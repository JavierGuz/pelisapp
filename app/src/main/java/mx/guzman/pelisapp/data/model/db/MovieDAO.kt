package mx.guzman.pelisapp.data.model.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MovieDAO {

    @Insert
    suspend fun insertMovie(movie: Movie): Long

    @Insert
    fun insertMovies(movies: List<Movie>): List<Long>

    @Query("DELETE FROM movie_data_table")
    suspend fun deleteAll(): Int

    @Query("SELECT * FROM movie_data_table")
    fun getAllMovies(): LiveData<List<Movie>>

    @Query("SELECT * FROM movie_data_table ORDER BY movie_popularity DESC")
    fun getAllPopularMovies(): List<Movie>
}