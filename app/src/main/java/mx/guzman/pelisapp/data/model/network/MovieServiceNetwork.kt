package mx.guzman.pelisapp.data.model.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response


class MovieServiceNetwork {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getMoviesNetwork(): MovieModelNetwork{
        return withContext(Dispatchers.IO){
            var response: Response<MovieModelNetwork> = retrofit.create(MovieApiClient::class.java).getAllMovies()
            response.body() ?: MovieModelNetwork(0,0,0, emptyList())
        }
    }
}