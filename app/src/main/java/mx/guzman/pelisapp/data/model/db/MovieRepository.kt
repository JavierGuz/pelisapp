package mx.guzman.pelisapp.data.model.db

import androidx.lifecycle.LiveData

class MovieRepository(private val dao: MovieDAO) {

    var movies = dao.getAllMovies()

    suspend fun insert(movie: Movie): Long{
        return dao.insertMovie(movie)
    }
    suspend fun insertAll(movies:List<Movie>):List<Long>{
        return dao.insertMovies(movies)
    }
    suspend fun deleteAll(): Int{
        return dao.deleteAll()
    }
    suspend fun getAllPopularMovies(): List<Movie> {
        return dao.getAllPopularMovies()
    }


}