package mx.guzman.pelisapp.data.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movie_data_table")
data class Movie(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "movie_id")
    var id:Int,

    @ColumnInfo(name = "movie_uniq")
    var uniq:Int,

    @ColumnInfo(name = "movie_media_type")
    var media_type:String,

    @ColumnInfo(name = "movie_overview")
    var overview:String,

    @ColumnInfo(name = "movie_popularity")
    var popularity:Float,

    @ColumnInfo(name = "movie_poster_path")
    var poster_path:String,

    @ColumnInfo(name = "movie_video")
    var video:String,

    @ColumnInfo(name = "movie_title")
    var title:String,

    @ColumnInfo(name = "movie_vote_average")
    var vote_average:Float,

    @ColumnInfo(name = "movie_vote_count")
    var vote_count:Float,


)