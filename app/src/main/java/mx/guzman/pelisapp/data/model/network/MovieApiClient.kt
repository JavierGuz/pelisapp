package mx.guzman.pelisapp.data.model.network


import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface MovieApiClient {

    @GET("4/list/1?api_key=fcbb53c75d293fd396e2dbd2139b7747")
    suspend fun getAllMovies(): Response<MovieModelNetwork>

}