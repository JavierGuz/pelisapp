package mx.guzman.pelisapp.data.model.network

class GetMoviesNetwork {
    private val repository = MovieRepositoryNetwork()

    suspend operator fun invoke():MovieModelNetwork?= repository.getAllMovies()
}