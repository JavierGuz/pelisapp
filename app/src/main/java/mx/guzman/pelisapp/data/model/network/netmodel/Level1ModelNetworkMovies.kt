package mx.guzman.pelisapp.data.model.network.netmodel

import com.google.gson.annotations.SerializedName

class Level1ModelNetworkMovies (


        @SerializedName("id") val id:Int,
        @SerializedName("media_type") val media_type:String,
        @SerializedName("overview") val overview:String,
        @SerializedName("popularity") val popularity:Float,
        @SerializedName("poster_path") val poster_path:String,
        @SerializedName("title") val title:String,
        @SerializedName("video") val video:String,
        @SerializedName("vote_average") val vote_average:Float,
        @SerializedName("vote_count") val vote_count:Float

    )