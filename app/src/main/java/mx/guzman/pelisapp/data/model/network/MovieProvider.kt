package mx.guzman.pelisapp.data.model.network

import mx.guzman.pelisapp.data.model.network.netmodel.Level1ModelNetworkMovies

class MovieProvider {
    companion object{
        var movies: List<Level1ModelNetworkMovies> = emptyList()
    }
}